<!-- Homework 4 skeleton -->
<html>
<body>
<?php
  echo "<h3>Homework 4</h3><br/>";
  
  /* Display your name */
  echo "<b>Name:</b> ";
  $firstname = 'David';
  $lastname = 'Mapa';
  echo "$firstname $lastname<br/><br/>";		/* Method 1.b */
      
  /* Assign file name to infile variable */
  $infile = "hw3.txt";

  $fp = fopen($infile,'r'); //Open file for reading
  
  if(!$fp){ // Test if the file cannot be opened
	  echo "<p> File <b>$infile</b> cannot be opened.</p>";
	  exit;
  }
  
  /* This while loop reads the file and assigns the appropriate data to the key or value array variable */  
  while (!feof($fp))
  {
	$readline = fgets($fp);   			// Read 1st line from the file
	$key = substr($readline,0,3); 		// Remove the first 3 characters from each read line for the key
	$value = substr($readline,3); 		// The remaining data after the key value is the value portion of the line
	if (strlen(trim($key)) >= 1) {		// This if statement assures only data with a valid key is assigned to the array
		$hw4array[$key]=$value;			// Assign key and value to array
	}	
  }
  
  asort($hw4array);						// The asort functions sorts an array with a descriptive key
  
  echo "<br>";
  echo "<h3>List of data after being sorted</h3><br>";
 ?>
<table border="0" cellpadding="5">
    <tr>
      <th> Key </th>
      <th> Value </th>
	</tr>
<?php
  while (list($key, $value) = each($hw4array))
  {
?> 
   <tr>
      <td> <?= $key ?> </td>
      <td> <?= $value ?> </td>
	</tr>
<?php	
	
  }
?>
</table>
<?php
    
  # Close the file since we are done.
  fclose($fp);  // Close file
  exit;
?>
</body>
</html>